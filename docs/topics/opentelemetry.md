# OpenTelemetry 

## Overview

### OpenTelemetry

> OpenTelemetry is a set of APIs, SDKs, tooling and integrations that are designed for the creation and management of telemetry data such as traces, metrics, and logs. 

- [Website](https://opentelemetry.io/)
- [Documentation](https://opentelemetry.io/docs/)

#### Facts

- Bring your own backend. OpenTelemetry provides the specification and collector only. 
    - KubeCon NA 2021: [OpenTelemetry Collector Deployment Patterns](https://www.youtube.com/watch?v=WhRrwSHDBFs) 
- Support for traces, metrics, logs. 
- [Prometheus vs. OpenTelemetry metrics](https://www.timescale.com/blog/prometheus-vs-opentelemetry-metrics-a-complete-guide/)

#### Hot Topics

- [Observability with OpenTelemetry](https://trstringer.com/otel-part1-intro/) is a great learning series by Thomas Stringer in 6 parts, covering [Introduction](https://trstringer.com/otel-part1-intro/), [Instrumentation](https://trstringer.com/otel-part2-instrumentation/), [Exporting](https://trstringer.com/otel-part3-export/), [Collector](https://trstringer.com/otel-part4-collector/), [Propagation](https://trstringer.com/otel-part5-propagation/), [Ecosystem](https://trstringer.com/otel-part6-ecosystem/).
- [Learn how to instrument an Apache HTTP Server with OpenTelemetry](https://opentelemetry.io/blog/2022/instrument-apache-httpd-server/)
- [OpenTelemetry for Python Developers](https://www.youtube.com/watch?v=RJeJWdTGieM)
- [Learn OpenTelemetry tracing using a lightweight microservice project](https://www.timescale.com/blog/learn-opentelemetry-tracing-with-this-lightweight-microservices-demo/)
- Logz.io created a great learning series for OpenTelemetry getting started in various languages:
    - [Beginner's Guide to OpenTelemetry](https://logz.io/learn/opentelemetry-guide/)
    - [Auto-Instrumenting Ruby Apps with OpenTelemetry](https://logz.io/blog/ruby-opentelemetry-auto-instrumentation/)
    - [Auto-Instrumenting Python Apps with OpenTelemetry](https://logz.io/blog/python-opentelemetry-auto-instrumentation/) 
    - [Instrumentation for C# .NET Apps with OpenTelemetry](https://logz.io/blog/csharp-dotnet-opentelemetry-instrumentation/)
- [How auto-instrumentation can help](https://www.honeycomb.io/blog/what-is-auto-instrumentation/)    

#### CI/CD Observability

- [tracepusher with OpenTelemetry](https://github.com/agardnerIT/tracepusher)
- [CI/CD Observability: Tracing with OpenTelemetry](https://gitlab.com/gitlab-org/gitlab/-/issues/338943) (GitLab proposal)

##### tracepusher

[tracepusher](https://github.com/agardnerIT/tracepusher) is a new Open Source project that provides a Python script to send traces to [OpenTelemetry](https://o11y.love/topics/opentelemetry/). The script can be run standalone on the command-line or integrated into GitLab CI/CD pipelines. The CI/CD configuration needs to be extended with a preparation job to generate a unique trace ID for the pipeline run, and consecutive `before_script` and `after_script` default definitions for all jobs to generate span IDs, and emit the trace span to OpenTelemetry with the `tracepusher` script, with a post pipeline job that finalizes the trace. You can create a quick demo setup with an OpenTelemetry collector that accepts HTTP messages using [this project which docker-compose](https://go.gitlab.com/t4zUp2). 

You can then follow my public learning curve implementing tracepusher into the slow pipeline project in [this MR](https://gitlab.com/gitlab-de/use-cases/observability/devsecops-efficiency/slow-pipeline-for-analysis/-/merge_requests/1). I also created upstream pull requests and issues, including [emphasis on the CI-tagged image](https://github.com/agardnerIT/tracepusher/pull/35), [jobs that specify a custom image](https://github.com/agardnerIT/tracepusher/issues/36), [before_script/after_script override problem workarounds](https://github.com/agardnerIT/tracepusher/issues/39) and [Jobs with needs do not download dotenv artifacts by default, causing new trace IDs generated](https://github.com/agardnerIT/tracepusher/issues/40).