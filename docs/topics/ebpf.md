# eBPF

## Overview

> Instead of relying on static counters and gauges exposed by the operating system, eBPF enables the collection & in-kernel aggregation of custom metrics and generation of visibility events based on a wide range of possible sources. 

- [ebpf.io](https://ebpf.io/)
    - [Infrastructure](https://ebpf.io/infrastructure) including the Linux Kernel, Compilers (LLVM, gcc), Libraries (Go, C/C++, Rust)
    - [Applications](https://ebpf.io/applications)

## Projects

### Observability

- [Pixie](https://px.dev/), Kubernetes observability for developers, auto-instrumented, scriptable.   
- [Coroot](https://coroot.com/), Kubernetes Observability, implements service maps using eBPF. 
- [Parca](https://www.parca.dev/), Continuous Profiling 
- [ebpf_exporter](https://github.com/cloudflare/ebpf_exporter), Prometheus exporter for custom eBPF metrics 
- [OpenTelemetry eBPF Collectors](https://github.com/open-telemetry/opentelemetry-ebpf), low level kernel telemetry data on a host Kernel, from the cloud or within a Kubernetes cluster.
- [Kepler (Kubernetes Efficient Power Level Exporter)](https://github.com/sustainable-computing-io/kepler) uses eBPF to probe energy-related system stats and exports them as Prometheus metrics.

### Security 

- [Cilium](https://cilium.io/), network connectivity security and observability. [The Cilium Story So Far](https://cloudnativenow.com/features/the-cilium-story-so-far/), April 2023.
    - [Tetragon](https://github.com/cilium/tetragon)
- [Tracee](https://aquasecurity.github.io/tracee/latest/), Runtime Security and Forensics
- [Falco](https://falco.org/), Kubernetes threat detection engine. Use case example: Package dependency scanning with [GitLab Package Hunter](https://falco.org/blog/gitlab-falco-package-hunter/)
- [Deepfence ThreatMapper](https://github.com/deepfence/ThreatMapper), Runtime Threat Management and Attack Path Enumeration for Cloud Native 

Security PoCs:

- [Boopkit](https://github.com/krisnova/boopkit), Linux eBPF backdoor over TCP. Spawn reverse shells, RCE, on prior privileged access.

### SRE/DevOps 

- [Inspektor Gadget](https://www.inspektor-gadget.io/), A collection of eBPF-based gadgets to debug and inspect Kubernetes apps and resources 
- [Caretta](https://github.com/groundcover-com/caretta), instant Kubernetes service dependency map in Grafana, using VictoriaMetrics as backend. 
- [BumbleBee](https://github.com/solo-io/bumblebee), build, run and distribute eBPF programs using OCI images.
- [q](https://github.com/krisnova/q), surface linux networking metrics with eBPF by Kris Nova. 

Zero code instrumentation:

- [Odigos](https://github.com/keyval-dev/odigos) provides distributed tracing without code changes. Instantly monitor any application using OpenTelemetry and eBPF.
- [Deepflow](https://deepflow.io/) implemented Zero Code data collection with eBPF for metrics, distributed tracing, request logs and function profiling. 

## Hot Topics

- [Past, Present, & Future of eBPF in Cloud Native Observability - Frederic Branczyk & Natalie Serrino](https://www.youtube.com/watch?v=pb_eAVAWq2o), KubeCon EU 2023. 
- [The Power of eBPF for Cloud Native Systems](https://cybersecurity-magazine.com/the-power-of-ebpf-for-cloud-native-systems/) is a comprehensive deep-dive into cloud-native, IoT and Edge computing, and ideas how to monetize eBPF. Suggest diving into [eBPF and its capabilities](https://medium.com/exness-blog/ebpf-and-its-capabilities-9a3a1dce3802). 
- [eBPF: Why now, introduction and deep dive](https://whynowtech.substack.com/p/ebpf?sd=pf)
- [eBPF report by Liz Rice](https://isovalent.com/ebpf/)
- [Bypassing eBPF-based Security Enforcement Tools](https://www.form3.tech/engineering/content/bypassing-ebpf-tools)
- [eCHO Episode 93: BPF Signing](https://www.youtube.com/watch?v=8mTWsFUAURE)

### Use cases 

- [Learn how eBPF can help minimize "observability tax"](https://coroot.com/blog/minimizing-observability-tax)
- [Distributed patterns compared: Frameworks vs. K8s vs. Service Mesh vs. eBPF](https://www.youtube.com/watch?v=uV_mS2c93Q8) by Matthias Haeussler and Tiffany Jernigan at Devoxx UK 
- [Hello eBPF! Goodbye Sidecars](https://www.youtube.com/watch?v=ThtRT8dhu8c) by [Liz Rice](https://www.linkedin.com/in/lizrice/)
- [Life Without Sidecars - Is eBPF's Promise Too Good to Be True?](https://www.youtube.com/watch?v=onQuRBy5rgo)


## Learning Resources

- [Learning eBPF book by Liz Rice](https://www.oreilly.com/library/view/learning-ebpf/9781098135119/), published March 2023.
- [Learning eBPF tutorial by Isovalent](https://isovalent.com/labs/learning-ebpf-tutorial/)
- [Learning eBPF for better Observability workshop at Cloudland 2023](../learning-resources.md#learning-ebpf-for-better-observability-at-cloudland-2023)
- [Learning eBPF for better Observability article on InfoQ.com](https://go.gitlab.com/f8rXxy)  - learning experience step-by-step by Michael Friedrich, published May 2023.
- [Debugging Production: eBPF Chaos article on InfoQ.com](https://go.gitlab.com/X5nXJ3) - eBPF use cases for production, and how to verify reliability with chaos engineering by Michael Friedrich, published June 2023.
- eBPF learning story shared by Michael Friedrich in their talk ["From Monitoring to Observability: eBPF Chaos" at Config Management Camp 2023](https://go.gitlab.com/5vhjv1).
- [Capture The Flag Challenges for eBPF Summit 2022](https://github.com/isovalent/eBPF-Summit-2022-CTF)
- [awesome-ebpf](https://github.com/zoidbergwill/awesome-ebpf)


### Newsletters

- [eCHO newsletter](https://cilium.io/newsletter/)
    - [eCHO YouTube livestreams](https://www.youtube.com/@eBPFCilium)
- [opsindev.news newsletter](https://opsindev.news/)
    - [The inner dev learning eBPF since Feb 2023](https://opsindev.news/archive/2023-02-21/#the-inner-dev-learning-ebpf) 

### Books and blog posts

- [Learning eBPF](https://www.oreilly.com/library/view/learning-ebpf/9781098135119/) by [Liz Rice](https://www.linkedin.com/in/lizrice/).
- [BPF Performance Tools (Book)](https://www.brendangregg.com/bpf-performance-tools-book.html)
- [How we diagnosed and resolved Redis latency spikes with BPF and other tools](https://about.gitlab.com/blog/2022/11/28/how-we-diagnosed-and-resolved-redis-latency-spikes/) is a thorough learning walkthrough from a problem, analysis, attempts, to final solutions. 
- [BlackHat Arsenal 2022: Detecting Linux kernel rootkits with Aqua Tracee](https://www.youtube.com/watch?v=EATX8g3sh-0)
- [Measuring CPU usage of eBPF programs with Inspektor Gadget](https://www.inspektor-gadget.io/blog/2022/10/measuring-cpu-usage-of-ebpf-programs-with-inspektor-gadget/)


### Development 

- [Learning eBPF Tracing: Tutorials and Examples (2019)](https://www.brendangregg.com/blog/2019-01-01/learn-ebpf-tracing.html) **recommended**
- [bpftrace](https://github.com/iovisor/bpftrace#)
- [bcc (BPF Compiler Collection)](https://github.com/iovisor/bcc#bpf-compiler-collection-bcc)´
- [libbpf-bootstrap](https://github.com/libbpf/libbpf-bootstrap): Examples that provide different use cases, for example traffic monitoring using XDP, written in Rust. 
- [An eBPF tutorial to try out the bpftrace framework](https://www.techtarget.com/searchitoperations/tutorial/An-eBPF-tutorial-to-try-out-the-bpftrace-framework)
- [The art of writing eBPF programs: a primer.](https://sysdig.com/blog/the-art-of-writing-ebpf-programs-a-primer/)
- [Getting Started on Kubernetes observability with eBPF](https://medium.com/@isalapiyarisi/getting-started-on-kubernetes-observability-with-ebpf-88139eb13fb2)
- [buzzer](https://github.com/google/buzzer), a fuzzer toolchain to write eBPF fuzzing strategies. These generate random eBPF programs and then validate that they do not have unexpected behavior in running on a Linux Kernel.

#### Development Tools

- [eBPF explorer](https://github.com/ebpfdev/explorer) is Web UI that lets you see all the maps and programs in eBPF subsystem. 

#### Development: XDP

Everything focussed on network communication and [XDP (eXpress Data Path)](https://en.wikipedia.org/wiki/Express_Data_Path).

- [Writing an eBPF/XDP load-balancer in Rust](https://konghq.com/blog/engineering/writing-an-ebpf-xdp-load-balancer-in-rust)
- [Building an XDP eBPF Program with C and Golang: A Step-by-Step Guide](https://dev.to/pemcconnell/building-an-xdp-ebpf-program-with-c-and-golang-a-step-by-step-guide-4hoa)
- [BPFAgent: eBPF for Monitoring at DoorDash](https://doordash.engineering/2023/08/15/bpfagent-ebpf-for-monitoring-at-doordash/)

#### Testing and CI/CD

- [Unit Testing eBPF Programs](https://who.ldelossa.is/posts/unit-testing-ebpf/) 
- [BPF CI sustem discussion at Linux Kernel Developers' bpfconf 2023](http://vger.kernel.org/bpfconf.html)

#### CO-RE (Compile Once, Run Everywhere)

- [The Challenge with Deploying eBPF Into the Wild](https://blog.px.dev/ebpf-portability/)
- [Andrii Nakryiko: BPF CO-RE reference guide](https://nakryiko.com/posts/bpf-core-reference-guide/)
- [Andrii Nakryiko: BPF CO-RE (Compile Once – Run Everywhere)](https://nakryiko.com/posts/bpf-portability-and-co-re/)

#### Debugging Tips

- [Elastic blog: Code coverage for eBPF programs](https://www.elastic.co/blog/code-coverage-for-ebpf-programs)
- [Andrii Nakryiko: Guide to bpf_trace_printk() and bpf_printk()](https://nakryiko.com/posts/bpf-tips-printk/)


#### eBPF Libraries

- [cilium/ebpf-go](https://github.com/cilium/ebpf) (Go) - [Use case examples](https://github.com/cilium/ebpf/tree/master/examples)
- [aquasecurity/libbpfgo](https://github.com/aquasecurity/libbpfgo) (Go)
    - [Used by the Parca agent](https://github.com/parca-dev/parca-agent/pull/901)
- [libbpf](https://github.com/libbpf/libbpf) (C/C++)
    - Wrapped by [aquasecurity/libbpfgo](https://github.com/aquasecurity/libbpfgo)
    - [libbpf-rs](https://github.com/libbpf/libbpf-rs) (Rust)
- [aya-rs](https://github.com/aya-rs/aya) (Rust)
    - [Program lifecycle](https://aya-rs.dev/book/aya/lifecycle/)
    - Used by [ebpfguard](https://github.com/deepfence/ebpfguard) to [implement Linux security policies](https://www.deepfence.io/blog/aya-your-trusty-ebpf-companion).
    - [Parca agent used Aya but migrated to libbpf](https://github.com/parca-dev/parca-agent/pull/869)

### Platforms 

- [Get started with eBPF using BumbleBee](https://www.solo.io/blog/get-started-with-ebpf-using-bumblebee/)  

## Events

- [eBPF Summit 2022 summary in the opsindev.news newsletter](https://opsindev.news/archive/2022-10-15/#ebpf-summit)
- [eBPF day at KubeCon EU 2022, summary in the opsindev.news newsletter](https://opsindev.news/archive/2022-06-13/#kubecon-eu)

### Meetups 

- [54. #EveryoneCanContribute Cafe: Pixie for Kubernetes Observability](https://everyonecancontribute.com/post/2022-09-13-cafe-54-pixie-for-kubernetes-observability/)
- [52. #EveryoneCanContribute Cafe: Learned at KubeCon EU, feat. Cilium Tetragon first try](https://everyonecancontribute.com/post/2022-06-16-cafe-52-learned-at-kubecon-eu-coffee-chat/)
- [49. #EveryoneCanContribute Cafe: Aqua Security and Open Source](https://everyonecancontribute.com/post/2022-03-08-cafe-49-aqua-security-open-source/)
- [42. #EveryoneCanContribute cafe: Falco and GitLab Package Hunter](https://everyonecancontribute.com/post/2021-08-11-cafe-42-falco-gitlab-package-hunter/)
- [32. #EveryoneCanContribute cafe: Continuous Profiling with Polar Signals](https://everyonecancontribute.com/post/2021-06-02-cafe-32-polar-signals-continuous-profiling/)